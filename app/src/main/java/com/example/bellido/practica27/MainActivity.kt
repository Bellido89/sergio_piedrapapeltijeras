package com.example.bellido.practica27

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(),RadioGroup.OnCheckedChangeListener {


    var selecUsuario : Int = 0
    var selecDispositivo : Int = 0
    var pntUsuario : Int = 0
    var pntDespositivo : Int = 0
    var pntEmpate : Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        grupoUsuario.setOnCheckedChangeListener(this)



    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

        if (checkedId == usuarioPiedra.id){

                selecUsuario = 1
                numRandom()
                opcionDispositivo()
                comparar()


        } else if (checkedId == usuarioPapel.id){
            selecUsuario = 2
            numRandom()
            opcionDispositivo()
            comparar()

        } else if (checkedId == usuarioTijeras.id){
            selecUsuario = 3
            numRandom()
            opcionDispositivo()
            comparar()

        }

    }



    fun numRandom(){
        selecDispositivo = Random().nextInt(4-1) + 1
    }

    fun opcionDispositivo(){
        if (selecDispositivo == 1){
            dispositivoPiedra.isChecked = true;
        } else if (selecDispositivo == 2){
            dispositivoPapel.isChecked = true;
        } else if (selecDispositivo ==3){
            dispositivoTijeras.isChecked = true;
        }
    }

    fun comparar() {
        if (selecUsuario == 1 && selecDispositivo == 1) {
            Toast.makeText(applicationContext, "Se ha producido un empate", Toast.LENGTH_SHORT).show()
            pntEmpate += 1
            contEmpate.setText(pntEmpate.toString())
        } else if (selecUsuario == 1 && selecDispositivo == 2) {
            Toast.makeText(applicationContext, "El dispositivo gana", Toast.LENGTH_SHORT).show()
            pntDespositivo += 1
            contDispositivo.setText(pntDespositivo.toString())
        } else if (selecUsuario == 1 && selecDispositivo == 3) {
            Toast.makeText(applicationContext, "El usuario gana", Toast.LENGTH_SHORT).show()
            pntUsuario += 1
            contUsuario.setText(pntUsuario.toString())
        }

        else if (selecUsuario == 2 && selecDispositivo == 1) {
            Toast.makeText(applicationContext, "El usuario gana", Toast.LENGTH_SHORT).show()
            pntUsuario += 1
            contUsuario.setText(pntUsuario.toString())
        } else if (selecUsuario == 2 && selecDispositivo == 2) {
            Toast.makeText(applicationContext, "Se ha producido un empate", Toast.LENGTH_SHORT).show()
            pntEmpate += 1
            contEmpate.setText(pntEmpate.toString())
        } else if (selecUsuario == 2 && selecDispositivo == 3) {
            Toast.makeText(applicationContext, "El dispositivo gana", Toast.LENGTH_SHORT).show()
            pntDespositivo += 1
            contDispositivo.setText(pntDespositivo.toString())
        }

        else if (selecUsuario == 3 && selecDispositivo == 1) {
            Toast.makeText(applicationContext, "El dispositivo gana", Toast.LENGTH_SHORT).show()
            pntDespositivo += 1
            contDispositivo.setText(pntDespositivo.toString())
        } else if (selecUsuario == 3 && selecDispositivo == 2) {
            Toast.makeText(applicationContext, "El usuario gana", Toast.LENGTH_SHORT).show()
            pntUsuario += 1
            contUsuario.setText(pntUsuario.toString())
        } else if (selecUsuario == 3 && selecDispositivo == 3) {
            Toast.makeText(applicationContext, "Se ha producido un empate", Toast.LENGTH_SHORT).show()
            pntEmpate += 1
            contEmpate.setText(pntEmpate.toString())
        }
    }


}
